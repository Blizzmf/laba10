#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define Input_all
//#define READ_FROM_FILE
#define READ_FROM_FILE_ALL
//#define bla

#ifdef READ_FROM_FILE_ALL
#define FILE_NAME "firms_all"
#else
#define FILE_NAME "firms"
#endif
struct Date{
    int day;
    int mm;
    int year;

};
typedef struct {
    char name[20];
    long ynn;
    char Fio[20];
    struct Date date;
} Firm;

FILE* InputStreem;

int checkNum(char* str, void* value){
    if(sscanf(str,"%d", (int*)value)!=1){
        return -1;
    }

    return 0;
}
int checkName(char* str, void* value){
    if(sscanf(str,"%s", (char*)value)!=1){
        return -1;
    }

    return 0;
}
int checkUNN(char* str, void* value){
    if(sscanf(str,"%ld", (long*)value)!=1){
        return -1;
    }

    return 0;
}
int checkFIO(char* str, void* value){
    char* e=str;
    char* dst=(char*) value;
    while(*e!='\0'){
        if(!(isalpha(*e)||isspace(*e)))
            return -1;
        e++;
    }
    char n, m;
    if(sscanf(str,"%s %c %c", dst, &n, &m)!=3){
      //  printf("%s %c %c", value, n, m);
        return -1;
    }
    sprintf(dst, "%s %c %c", dst, n, m);
    //printf("%s\n",dst);

    return 0;
}
int checkDate(char* str, void* value){
    struct Date* date = (struct Date*) value;
    if(sscanf(str,"%d %d %d", &date->day, &date->mm, &date->year)!=3){
        return -1;
    }
    if(date->day<1||date->day>31)
        return -1;
    if(date->mm<1||date->mm>12)
        return -1;

    return 0;
}
checkInput(char* str, void* value){
	Firm* firm = (Firm*) value;
	char * e = str;
	e = strchr(e, '"');
	if(!e) return -1;
	e++;
	str = strchr(e, '"');
	if (!str) return -1;
	*str='\0';
	str++;
	if (checkName(e, firm->name))
		return -1;
    if(sscanf(str," [%ld]",&firm->ynn)!=1)
		return -1;
	e = strchr(str, ',');
	if (!e)
		return -1;
	e++;
	str = strchr(e, ',');
	if (!str)
		return -1;
	*str = '\0';
	str++;
	if (checkFIO(e, firm->Fio))
		return -1;
	if (checkDate(str, &firm->date))
		return -1;
    return 0;
}
void readData(char* msg, int (*check)(char*, void*), void* value){
    char* str = (char*) calloc(128, sizeof(char));
    if(InputStreem==stdin)
        printf("%s\n", msg);
//    fflush(stdin);
    __fpurge(stdin);
    while(1){
        //gets(str);
        fgets(str, 128, InputStreem);
        if(check(str, value)){
            if(InputStreem==stdin)
                printf("Ошибка ввода, повторите ввод\n");
        }else{
            break;
        }
    }
    free(str);
}

int compareByFio(Firm* f1, Firm* f2)
{
	return strcmp(f1->Fio, f2->Fio);
}
int compareByDate(Firm* f1, Firm* f2)
{
	//struct Date* date = (struct Date*) value;
	int result;
	if((result = (f1->date.year-f2->date.year))!= 0)
	{
		return result;
	}
	else if((result = (f1->date.mm-f2->date.mm))!= 0)
	{
		return result;
	}
	else
	{
		return (f1->date.day-f2->date.day);
	}

}
int compare(Firm* f1, Firm* f2)
{
	int result;
	result = compareByFio(f1, f2);
	if(result == 0)
	{
		result = compareByDate(f1, f2);
	}
	return result;
}
void bubbleSort(Firm** firm, int size)
{
	int i, j;
	Firm* tmp;
	for(i = 0; i < size; i++)
	{
		for(j = 0; j < size - 1; j++)
		{
			if(compare(firm[j], firm[j + 1]) > 0)
			{
				tmp = firm[j];
				firm[j] = firm[j + 1];
				firm[j + 1] = tmp;
			}
		}
	}
}
Firm* Input(int id){
    Firm * firm;
    firm=(Firm*)malloc(sizeof(Firm));
    //printf("Введите название фирмы №%d, УНН, ФИО, дата основания фирмы: ",id);
    //gets(firm->name);
    #ifdef Input_all
    readData("Введите данны о фирме('Название' [УНН], ФИО, Дата основание",checkInput,firm);
    #else
    readData("Введите название фирмы: ", checkName, firm->name);
    readData("Введите UNN: ", checkUNN, &firm->ynn);
    readData("Введите ФИО: ", checkFIO, &firm->Fio);
    readData("Введите дату основания фирмы: ", checkDate, &firm->date);
    #endif // Input_all
    //scanf("%ld",&firm->ynn);
    //getchar();
    //gets(firm->Fio);
  //  scanf("%d %d %d", &firm->date.day, &firm->date.mm, &firm->date.year);
    //free(firm);
    return firm;
}
void printFirm(Firm * firms){
    printf("%20s %10ld %20s %02d-%02d-%04d\n",firms->name,firms->ynn,firms->Fio,firms->date.day,firms->date.mm,firms->date.year);
}
void strToLower(char *c1, char* c2){
	int len = strlen(c1);

	int i;
	for(i = 0; i<= len; i++){
		c2[i] = tolower(c1[i]);
	}
}
int searchByName(Firm** f1, int size, Firm** f2, char* val){
    int i, j=0;
    strToLower(val, val);
    char str[20];
    for(i=0; i<size; i++){
        strToLower(f1[i]->name, str);
#ifdef bla
        if((strstr(str, val)!=NULL)){
#else
        if((strstr(str, val)==NULL)){
#endif
            f2[j]=f1[i];
            j++;
        }
    }
    return j;
}

int main(int argc, char *argv[])
{
    int n,id;
//#ifdef READ_FROM_FILE
#ifdef READ_FROM_FILE_ALL
    FILE* ifile;
    ifile = fopen(FILE_NAME, "r");
    if(!ifile)
        exit(-1);
    InputStreem = ifile;
#else
    InputStreem = stdin;
#endif
    readData("Введите кол-во фирм : ", checkNum, &n);
    Firm * firms[n];
    Firm* s_firms[n];
    char str[20];
    for(id=0;id<n;id++){
        firms[id] = Input(id);
    }
    InputStreem = stdin;
    for(id=0;id<n;id++){

        printFirm(firms[id]);
    }
    bubbleSort(firms,n);
    printf("Отсортированный ...\n");
    for(id=0;id<n;id++){

        printFirm(firms[id]);
    }
    readData("Введите строку для поиска : ", checkName, str);
    int s_n = searchByName(firms, n, s_firms, str);
    for(id=0;id<s_n;id++){

        printFirm(s_firms[id]);
    }

    return 0;
}
